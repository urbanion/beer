package com.holoceo.beers

import android.app.Application
import com.holoceo.beers.di.ApplicationComponent
import com.holoceo.beers.di.DaggerApplicationComponent

class BeersApplication : Application() {

    private lateinit var applicationComponent: ApplicationComponent

    override fun onCreate() {
        super.onCreate()
        applicationComponent = DaggerApplicationComponent.factory().create(this)
    }

    fun getApplicationComponent(): ApplicationComponent = applicationComponent
}