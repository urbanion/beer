package com.holoceo.beers.domain.list

import io.reactivex.Observable
import javax.inject.Inject

class Store @Inject constructor(
    private val middleware: Middleware
) {

    @Volatile
    private var state = State()

    fun observeState(userActions: Observable<Action>): Observable<State> =
        middleware
            .process(userActions) { state }
            .scan(state) { state, action -> reduce(state, action) }
            .distinctUntilChanged()
            .doOnNext { newState -> state = newState }

    private fun reduce(state: State, action: Action): State =
        when (action) {
            is Action.LoadBeers -> reduceLoadBeers(state)
            is Action.BeersLoaded -> reduceBeersLoaded(state, action)
            is Action.Error -> reduceError(state, action)
        }

    private fun reduceLoadBeers(state: State): State =
        state.copy(loading = true)

    private fun reduceBeersLoaded(state: State, action: Action.BeersLoaded): State =
        state.copy(
            beers = state.beers + action.beers,
            loading = false,
            error = null,
            canLoadMore = action.canLoadMore,
            currentPage = state.currentPage + 1
        )

    private fun reduceError(state: State, action: Action.Error): State =
        state.copy(
            error = action.error,
            loading = false
        )
}