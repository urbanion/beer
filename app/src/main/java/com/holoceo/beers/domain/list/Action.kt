package com.holoceo.beers.domain.list

import com.holoceo.beers.domain.Beer

sealed class Action {
    object LoadBeers : Action()
    data class BeersLoaded(val beers: List<Beer>, val canLoadMore: Boolean) : Action()
    data class Error(val error: Throwable) : Action()
}