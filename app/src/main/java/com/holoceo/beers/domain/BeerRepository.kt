package com.holoceo.beers.domain

import io.reactivex.Single

interface BeerRepository {
    fun getBeer(id: Long): Single<Beer>
    fun getBeers(page: Int, count: Int): Single<List<Beer>>
}