package com.holoceo.beers.domain.list

import com.holoceo.beers.domain.Beer

data class State(
    val beers: List<Beer> = emptyList(),
    val currentPage: Int = 1,
    val loading: Boolean = false,
    val canLoadMore: Boolean = true,
    val error: Throwable? = null
)