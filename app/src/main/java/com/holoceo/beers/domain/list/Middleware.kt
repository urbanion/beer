package com.holoceo.beers.domain.list

import androidx.annotation.VisibleForTesting
import com.holoceo.beers.domain.BeerRepository
import io.reactivex.BackpressureStrategy
import io.reactivex.Observable
import javax.inject.Inject

class Middleware @Inject constructor(
    private val beerRepository: BeerRepository
) {

    fun process(
        userActions: Observable<Action>,
        stateProvider: () -> State
    ): Observable<Action> =
        Observable.merge(
            userActions,
            observeLoadedBeers(userActions, stateProvider)
        )

    private fun observeLoadedBeers(
        userActions: Observable<Action>,
        stateProvider: () -> State
    ): Observable<Action> =
        userActions
            .filter { it is Action.LoadBeers }
            .map { it as Action.LoadBeers }
            .startWith(Action.LoadBeers)
            .toFlowable(BackpressureStrategy.DROP)
            .flatMapSingle(
                { action ->
                    beerRepository
                        .getBeers(stateProvider().currentPage, BEERS_PER_PAGE)
                        .map<Action> { beers ->
                            Action.BeersLoaded(
                                beers = beers,
                                canLoadMore = beers.size == BEERS_PER_PAGE
                            )
                        }
                        .onErrorReturn { error ->
                            Action.Error(error)
                        }
                },
                false,
                1
            )
            .toObservable()

    companion object {

        @VisibleForTesting
        const val BEERS_PER_PAGE = 20
    }
}