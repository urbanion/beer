package com.holoceo.beers.domain

data class Beer(
    val id: Long,
    val name:  String,
    val tagline: String,
    val imageUrl: String,
    val description: String
)