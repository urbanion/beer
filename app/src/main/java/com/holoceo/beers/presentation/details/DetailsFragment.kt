package com.holoceo.beers.presentation.details

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.bumptech.glide.Glide
import com.holoceo.beers.BeersApplication
import com.holoceo.beers.R
import com.holoceo.beers.databinding.FragmentDetailsBinding
import com.holoceo.beers.domain.Beer
import com.holoceo.beers.presentation.ToolbarHolder
import com.holoceo.beers.presentation.ui_state.UiStateRenderer

class DetailsFragment : Fragment() {

    private var _binding: FragmentDetailsBinding? = null

    private val binding get() = _binding!!

    private val viewModel by lazy {
        arguments
        ViewModelProvider(this,
            DetailsViewModelFactory(
                application = requireActivity().application as BeersApplication,
                beerId = requireArguments().getLong(EXTRA_BEER_ID)
            )
        ).get(DetailsViewModel::class.java)
    }

    private val uiStateRenderer by lazy {
        UiStateRenderer<Beer>(
            context = requireContext(),
            progressView = binding.progress,
            errorView = binding.error,
            contentView = binding.content,
            errorMessage = binding.errorMessage,
            contentDisplayer = ::displayBeerDetails
        )
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentDetailsBinding.inflate(layoutInflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        (requireActivity() as ToolbarHolder).setBackButtonVisible(true)
        viewModel.getState().observe(viewLifecycleOwner, uiStateRenderer::render)
        binding.btnRetry.setOnClickListener { viewModel.onRetryClicked()}
    }

    private fun displayBeerDetails(beer: Beer) {
        Glide.with(this)
            .load(beer.imageUrl)
            .placeholder(R.drawable.placeholder)
            .into(binding.image)

        binding.title.text = beer.name
        binding.description.text = beer.description
    }

    override fun onDestroyView() {
        super.onDestroyView()
        uiStateRenderer.onDestroyView()
        _binding = null
    }

    companion object {
        private const val EXTRA_BEER_ID = "beer_id"

        fun getNewInstance(beerId: Long): DetailsFragment {
            return DetailsFragment().apply {
                arguments = Bundle().apply {
                    putLong(EXTRA_BEER_ID, beerId)
                }
            }
        }
    }
}