package com.holoceo.beers.presentation.list

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.holoceo.beers.domain.list.Action
import com.holoceo.beers.domain.list.State
import com.holoceo.beers.domain.list.Store
import com.holoceo.beers.presentation.ui_state.UiState
import com.holoceo.beers.presentation.error.ErrorMapper
import com.holoceo.beers.presentation.list.model.*
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.subjects.PublishSubject

class ListViewModel(
    store: Store,
    private val errorMapper: ErrorMapper,
    private val router: Router
) : ViewModel() {

    private val state = MutableLiveData<UiState<List<ListItem>>>()
    private val userActions = PublishSubject.create<Action>()
    private val stateDisposable: Disposable

    init {
        stateDisposable = store
            .observeState(userActions)
            .map(::toUiState)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(state::setValue)
    }

    fun getState(): LiveData<UiState<List<ListItem>>> = state

    fun onBeerClicked(item: BeerListItem) {
        router.showBeerDetails(item.beerId)
    }

    fun onRetryClicked() {
        userActions.onNext(Action.LoadBeers)
    }

    fun onLoadMore() {
        userActions.onNext(Action.LoadBeers)
    }

    override fun onCleared() {
        stateDisposable.dispose()
    }

    private fun toUiState(state: State): UiState<List<ListItem>> =
        when {
            state.beers.isNotEmpty() -> UiState.Content(toListItems(state))
            !state.loading && state.error != null -> UiState.Error(
                errorMapper.getErrorMessageResId(state.error)
            )
            else -> UiState.Loading
        }

    private fun toListItems(state: State): List<ListItem> {
        val items = mutableListOf<ListItem>()

        items += state.beers.map { beer ->
            BeerListItem(beer.id.toString(), beer.id, beer.imageUrl, beer.name, beer.tagline)
        }

        if (state.loading || state.error == null && state.canLoadMore) {
            items += LoadingListItem
        }

        if (!state.loading && state.error != null) {
            items += ErrorListItem(errorMapper.getErrorMessageResId(state.error))
        }

        return items
    }
}