package com.holoceo.beers.presentation.details

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.holoceo.beers.domain.Beer
import com.holoceo.beers.domain.BeerRepository
import com.holoceo.beers.presentation.error.ErrorMapper
import com.holoceo.beers.presentation.ui_state.UiState
import io.reactivex.BackpressureStrategy
import io.reactivex.disposables.Disposable
import io.reactivex.subjects.PublishSubject

class DetailsViewModel(
    beerId: Long,
    repository: BeerRepository,
    errorMapper: ErrorMapper
) : ViewModel() {

    private val state = MutableLiveData<UiState<Beer>>(UiState.Loading)
    private val loadEvents = PublishSubject.create<Unit>()
    private val beerDisposable: Disposable

    init {
        beerDisposable = loadEvents
            .startWith(Unit)
            .toFlowable(BackpressureStrategy.DROP)
            .flatMapSingle(
                { repository.getBeer(beerId) },
                false,
                1
            )
            .map<UiState<Beer>> { UiState.Content(it) }
            .onErrorReturn { error -> UiState.Error(errorMapper.getErrorMessageResId(error)) }
            .subscribe(state::postValue)
    }

    fun getState(): LiveData<UiState<Beer>> = state

    fun onRetryClicked() {
        state.value = UiState.Loading
        loadEvents.onNext(Unit)
    }

    override fun onCleared() {
        beerDisposable.dispose()
    }
}