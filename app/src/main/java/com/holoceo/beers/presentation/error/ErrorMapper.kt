package com.holoceo.beers.presentation.error

interface ErrorMapper {
    fun getErrorMessageResId(error: Throwable): Int
}