package com.holoceo.beers.presentation.list.adapter

import com.hannesdorfmann.adapterdelegates4.dsl.adapterDelegateViewBinding
import com.holoceo.beers.databinding.ItemLoadingBinding
import com.holoceo.beers.presentation.list.model.ListItem
import com.holoceo.beers.presentation.list.model.LoadingListItem

fun loadingAdapterDelegate(
    onBindListener: () -> Unit
) = adapterDelegateViewBinding<LoadingListItem, ListItem, ItemLoadingBinding>(
    { layoutInflater, root -> ItemLoadingBinding.inflate(layoutInflater, root, false) }
) {
    bind {
        onBindListener()
    }
}