package com.holoceo.beers.presentation.list

import androidx.recyclerview.widget.DiffUtil
import com.holoceo.beers.presentation.list.model.ListItem

object ListItemDiffCallback : DiffUtil.ItemCallback<ListItem>() {

    override fun areItemsTheSame(oldItem: ListItem, newItem: ListItem): Boolean =
        oldItem.id == newItem.id

    override fun areContentsTheSame(oldItem: ListItem, newItem: ListItem): Boolean =
        oldItem.hashCode() == newItem.hashCode()
}