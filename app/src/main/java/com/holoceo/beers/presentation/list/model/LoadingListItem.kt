package com.holoceo.beers.presentation.list.model

object LoadingListItem : ListItem {
    override val id: String = "loading"
}