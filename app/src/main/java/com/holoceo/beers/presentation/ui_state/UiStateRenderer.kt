package com.holoceo.beers.presentation.ui_state

import android.content.Context
import android.view.View
import android.view.animation.AnimationUtils
import android.widget.TextView

class UiStateRenderer<T : Any>(
    val context: Context,
    val progressView: View,
    val errorView: View,
    val contentView: View,
    val errorMessage: TextView,
    val contentDisplayer: (T) -> Unit
) {

    private var currentView: View? = null

    fun render(state: UiState<T>) =
        when (state) {
            is UiState.Loading -> changeView(progressView)
            is UiState.Error -> {
                errorMessage.setText(state.errorMessageResId)
                changeView(errorView)
            }
            is UiState.Content -> {
                changeView(contentView)
                contentDisplayer(state.content)
            }
        }

    fun onDestroyView() {
        currentView = null
    }

    private fun changeView(view: View) {
        if (currentView !== view) {
            currentView?.startAnimation(AnimationUtils.loadAnimation(context, android.R.anim.fade_out))
            view.startAnimation(AnimationUtils.loadAnimation(context, android.R.anim.fade_in))
            currentView?.visibility = View.GONE
            view.visibility = View.VISIBLE
            currentView = view
        }
    }
}