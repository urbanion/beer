package com.holoceo.beers.presentation.list

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.holoceo.beers.BeersApplication

class ListViewModelFactory(private val application: BeersApplication) : ViewModelProvider.Factory {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        val appComponent = application.getApplicationComponent()

        if (modelClass.isAssignableFrom(ListViewModel::class.java)) {
            return ListViewModel(
                store = appComponent.getStore(),
                errorMapper = appComponent.getErrorMapper(),
                router = appComponent.getRouter()
            ) as T
        } else {
            throw IllegalArgumentException("Unknown ViewModel class")
        }
    }
}