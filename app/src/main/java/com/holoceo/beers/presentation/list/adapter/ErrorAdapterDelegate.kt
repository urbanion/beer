package com.holoceo.beers.presentation.list.adapter

import com.hannesdorfmann.adapterdelegates4.dsl.adapterDelegateViewBinding
import com.holoceo.beers.databinding.ItemErrorBinding
import com.holoceo.beers.presentation.list.model.ErrorListItem
import com.holoceo.beers.presentation.list.model.ListItem

fun errorAdapterDelegate(
    retryListener: () -> Unit
) = adapterDelegateViewBinding<ErrorListItem, ListItem, ItemErrorBinding>(
    { layoutInflater, root -> ItemErrorBinding.inflate(layoutInflater, root, false) }
) {
    binding.btnRetry.setOnClickListener {
        retryListener()
    }
    bind {
        binding.description.setText(item.errorMessageResId)
    }
}