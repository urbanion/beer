package com.holoceo.beers.presentation

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.holoceo.beers.BeersApplication
import com.holoceo.beers.R

class MainActivity : AppCompatActivity(), ToolbarHolder {

    private val navigator by lazy {
        (application as BeersApplication).getApplicationComponent().getNavigator()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        navigator.fragmentManager = supportFragmentManager
        if (savedInstanceState == null) navigator.showBeerList()
    }

    override fun setBackButtonVisible(visible: Boolean) {
        supportActionBar?.apply {
            setDisplayHomeAsUpEnabled(visible)
            setDisplayShowHomeEnabled(visible)
        }
    }

    override fun onSupportNavigateUp(): Boolean {
        navigator.goBack()
        return true
    }

    override fun onDestroy() {
        navigator.fragmentManager = null
        super.onDestroy()
    }
}