package com.holoceo.beers.presentation.error

import com.holoceo.beers.R
import java.net.UnknownHostException
import javax.inject.Inject

class ErrorMapperImpl @Inject constructor(): ErrorMapper {

    override fun getErrorMessageResId(error: Throwable): Int =
        if (error is UnknownHostException) {
            R.string.error_no_internet
        } else {
            R.string.error_generic
        }
}