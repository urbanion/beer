package com.holoceo.beers.presentation

interface ToolbarHolder {
    fun setBackButtonVisible(visible: Boolean)
}