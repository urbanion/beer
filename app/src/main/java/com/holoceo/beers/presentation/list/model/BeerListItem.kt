package com.holoceo.beers.presentation.list.model

data class BeerListItem(
    override val id: String,
    val beerId: Long,
    val imageUrl: String,
    val title: String,
    val description: String
) : ListItem