package com.holoceo.beers.presentation.list.model

import androidx.annotation.StringRes

data class ErrorListItem(@StringRes val errorMessageResId: Int) : ListItem {
    override val id: String = "error"
}