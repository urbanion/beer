package com.holoceo.beers.presentation.list

interface Router {
    fun showBeerDetails(beerId: Long)
}