package com.holoceo.beers.presentation.ui_state

import androidx.annotation.StringRes

sealed class UiState<out T : Any> {
    object Loading : UiState<Nothing>()
    class Content<T : Any>(val content: T) : UiState<T>()
    class Error(@StringRes val errorMessageResId: Int) : UiState<Nothing>()
}