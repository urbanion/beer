package com.holoceo.beers.presentation

import androidx.fragment.app.FragmentManager
import com.holoceo.beers.R
import com.holoceo.beers.presentation.details.DetailsFragment
import com.holoceo.beers.presentation.list.ListFragment
import com.holoceo.beers.presentation.list.Router
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class Navigatior @Inject constructor() : Router {

    var fragmentManager: FragmentManager? = null

    fun showBeerList() {
        fragmentManager
            ?.beginTransaction()
            ?.add(R.id.container, ListFragment())
            ?.commit()
    }

    fun goBack() {
        fragmentManager?.popBackStack()
    }

    override fun showBeerDetails(beerId: Long) {
        fragmentManager
            ?.beginTransaction()
            ?.replace(R.id.container, DetailsFragment.getNewInstance(beerId))
            ?.addToBackStack(null)
            ?.commit()
    }
}