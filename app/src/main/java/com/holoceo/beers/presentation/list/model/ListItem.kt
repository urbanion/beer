package com.holoceo.beers.presentation.list.model

interface ListItem {
    val id: String
}