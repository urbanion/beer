package com.holoceo.beers.presentation.list

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.hannesdorfmann.adapterdelegates4.AsyncListDifferDelegationAdapter
import com.holoceo.beers.BeersApplication
import com.holoceo.beers.databinding.FragmentListBinding
import com.holoceo.beers.presentation.ToolbarHolder
import com.holoceo.beers.presentation.ui_state.UiStateRenderer
import com.holoceo.beers.presentation.list.adapter.beerAdapterDelegate
import com.holoceo.beers.presentation.list.adapter.errorAdapterDelegate
import com.holoceo.beers.presentation.list.adapter.loadingAdapterDelegate
import com.holoceo.beers.presentation.list.model.ListItem

class ListFragment : Fragment() {

    private var _binding: FragmentListBinding? = null

    private val binding get() = _binding!!

    private val viewModel by lazy {
        ViewModelProvider(this, ListViewModelFactory(requireActivity().application as BeersApplication))
            .get(ListViewModel::class.java)
    }

    private val adapter by lazy {
        AsyncListDifferDelegationAdapter(
            ListItemDiffCallback,
            beerAdapterDelegate(viewModel::onBeerClicked),
            loadingAdapterDelegate(viewModel::onLoadMore),
            errorAdapterDelegate(viewModel::onRetryClicked)
        )
    }

    private val uiStateRenderer by lazy {
        UiStateRenderer<List<ListItem>>(
            context = requireContext(),
            progressView = binding.progress,
            errorView = binding.error,
            contentView = binding.list,
            errorMessage = binding.errorMessage,
            contentDisplayer = adapter::setItems
        )
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentListBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        (requireActivity() as ToolbarHolder).setBackButtonVisible(false)
        binding.list.layoutManager = LinearLayoutManager(context)
        binding.list.adapter = adapter
        viewModel.getState().observe(viewLifecycleOwner, uiStateRenderer::render)
        binding.btnRetry.setOnClickListener { viewModel.onRetryClicked() }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        uiStateRenderer.onDestroyView()
        _binding = null
    }
}