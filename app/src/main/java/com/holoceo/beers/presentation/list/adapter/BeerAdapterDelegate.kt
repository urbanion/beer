package com.holoceo.beers.presentation.list.adapter

import com.bumptech.glide.Glide
import com.hannesdorfmann.adapterdelegates4.dsl.adapterDelegateViewBinding
import com.holoceo.beers.R
import com.holoceo.beers.databinding.ItemBeerBinding
import com.holoceo.beers.presentation.list.model.BeerListItem
import com.holoceo.beers.presentation.list.model.ListItem

fun beerAdapterDelegate(
    clickListener: (BeerListItem) -> Unit
) = adapterDelegateViewBinding<BeerListItem, ListItem, ItemBeerBinding>(
    { layoutInflater, root -> ItemBeerBinding.inflate(layoutInflater, root, false) }
) {
    binding.root.setOnClickListener {
        clickListener(item)
    }
    bind {
        binding.title.text = item.title
        binding.description.text = item.description
        Glide.with(binding.root)
            .load(item.imageUrl)
            .placeholder(R.drawable.placeholder)
            .into(binding.image)

    }
}