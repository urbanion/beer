package com.holoceo.beers.presentation.details

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.holoceo.beers.BeersApplication

class DetailsViewModelFactory(
    private val application: BeersApplication,
    private val beerId: Long
) : ViewModelProvider.Factory {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        val appComponent = application.getApplicationComponent()

        if (modelClass.isAssignableFrom(DetailsViewModel::class.java)) {
            return DetailsViewModel(
                repository = appComponent.getRepository(),
                errorMapper = appComponent.getErrorMapper(),
                beerId = beerId
            ) as T
        } else {
            throw IllegalArgumentException("Unknown ViewModel class")
        }
    }
}