package com.holoceo.beers.di

import com.google.gson.Gson
import com.holoceo.beers.data.BeerApi
import com.holoceo.beers.data.BeerRepositoryImpl
import com.holoceo.beers.domain.BeerRepository
import com.holoceo.beers.presentation.Navigatior
import com.holoceo.beers.presentation.error.ErrorMapper
import com.holoceo.beers.presentation.error.ErrorMapperImpl
import com.holoceo.beers.presentation.list.Router
import dagger.Binds
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@Module
abstract class ApplicationModule {

    @Singleton
    @Binds
    abstract fun bindErrorMapper(mapperImpl: ErrorMapperImpl): ErrorMapper

    @Singleton
    @Binds
    abstract fun bindsBeerRepository(repository: BeerRepositoryImpl): BeerRepository

    @Singleton
    @Binds
    abstract fun bindRouter(navigator: Navigatior): Router

    @Module
    companion object {

        @Singleton
        @Provides
        fun provideBeerApi(
            okHttpClient: OkHttpClient
        ) = Retrofit.Builder()
            .client(okHttpClient)
            .baseUrl("https://api.punkapi.com/v2/")
            .addConverterFactory(GsonConverterFactory.create(Gson()))
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .build()
            .create(BeerApi::class.java)

        @Singleton
        @Provides
        fun provideHttpClient(): OkHttpClient =
            OkHttpClient.Builder()
                .addInterceptor(
                    HttpLoggingInterceptor().apply {
                        level = HttpLoggingInterceptor.Level.BODY
                    }
                )
                .build()
    }
}