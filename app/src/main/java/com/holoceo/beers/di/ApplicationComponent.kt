package com.holoceo.beers.di

import com.holoceo.beers.BeersApplication
import com.holoceo.beers.domain.BeerRepository
import com.holoceo.beers.domain.list.Store
import com.holoceo.beers.presentation.Navigatior
import com.holoceo.beers.presentation.error.ErrorMapper
import com.holoceo.beers.presentation.list.Router
import dagger.BindsInstance
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [ApplicationModule::class])
interface ApplicationComponent {

    fun getRepository(): BeerRepository

    fun getStore(): Store

    fun getErrorMapper(): ErrorMapper

    fun getRouter(): Router

    fun getNavigator(): Navigatior

    @Component.Factory
    interface Factory {
        fun create(@BindsInstance instance: BeersApplication): ApplicationComponent
    }
}