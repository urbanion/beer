package com.holoceo.beers.data

import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface BeerApi {

    @GET("beers")
    fun getBeers(
        @Query("page") page: Int,
        @Query("per_page") count: Int
    ): Single<List<BeerDto>>

    @GET("beers/{id}")
    fun getBeer(
        @Path("id") beerId: Long
    ): Single<BeerDto>
}