package com.holoceo.beers.data

import com.google.gson.annotations.SerializedName

class BeerDto(
    @SerializedName("id") val id: Long,
    @SerializedName("name") val name:  String,
    @SerializedName("tagline") val tagline: String?,
    @SerializedName("image_url") val imageUrl: String?,
    @SerializedName("description") val  description: String?,
    @SerializedName("food_pairing") val foodPairing: List<String>?,
    @SerializedName("brewers_tips") val tips: String?
)