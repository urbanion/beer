package com.holoceo.beers.data

import com.holoceo.beers.domain.Beer
import com.holoceo.beers.domain.BeerRepository
import io.reactivex.Single
import io.reactivex.schedulers.Schedulers
import java.util.concurrent.ConcurrentHashMap
import javax.inject.Inject

class BeerRepositoryImpl @Inject constructor(private val api: BeerApi) : BeerRepository {

    //Making an assumption that loaded beers scope is bound to app scope
    private val cache = ConcurrentHashMap<Long, Beer>()

    override fun getBeer(id: Long): Single<Beer> {
        cache[id]?.let { return Single.just(it) }

        return api
            .getBeer(id)
            .map { it.toDomain() }
            .subscribeOn(Schedulers.io())
    }


    override fun getBeers(page: Int, count: Int): Single<List<Beer>> =
        api
            .getBeers(page, count)
            .map { beers ->
                beers.map { it.toDomain() }
            }
            .doOnSuccess { beers ->
                beers.forEach { beer ->
                    cache[beer.id] = beer
                }
            }
            .subscribeOn(Schedulers.io())


    private fun BeerDto.toDomain() = Beer(
        id = id,
        name = name,
        tagline = tagline ?: "",
        imageUrl = imageUrl ?: "",
        description = description ?: ""
    )
}