package com.holoceo.beers

import com.holoceo.beers.domain.Beer
import com.holoceo.beers.domain.list.Middleware

object BeerMocks {
    val BEER = Beer(1, "Name", "Tag", "Image", "Description")
    val BEERS_1 = listOf(BEER)
    val BEERS_PAGE = (1..Middleware.BEERS_PER_PAGE).map { BEER }
}