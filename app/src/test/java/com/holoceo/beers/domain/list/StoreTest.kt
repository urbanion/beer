package com.holoceo.beers.domain.list

import com.holoceo.beers.BeerMocks
import io.reactivex.subjects.PublishSubject
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import org.mockito.kotlin.any
import org.mockito.kotlin.mock
import org.mockito.kotlin.whenever
import java.net.UnknownHostException

class StoreTest {
    private val middleware = mock<Middleware>()
    private val actions = PublishSubject.create<Action>()
    private lateinit var store: Store

    @Before
    fun setUp() {
        whenever(middleware.process(any(), any())).thenReturn(actions)
        store = Store(middleware)
    }

    @Test
    fun `page index is increased after beers are loaded`() {
        val observer = store.observeState(actions).test()
        actions.onNext(
            Action.BeersLoaded(
                beers = BeerMocks.BEERS_PAGE,
                canLoadMore = true
            )
        )
        assertEquals(2, observer.values().last().currentPage)
    }

    @Test
    fun `beers are aggregated`() {
        val observer = store.observeState(actions).test()
        actions.onNext(
            Action.BeersLoaded(
                beers = BeerMocks.BEERS_PAGE,
                canLoadMore = true
            )
        )
        actions.onNext(
            Action.BeersLoaded(
                beers = BeerMocks.BEERS_PAGE,
                canLoadMore = true
            )
        )
        assertEquals(2 * BeerMocks.BEERS_PAGE.size, observer.values().last().beers.size)
    }

    @Test
    fun `state is updated when load event is emitted`() {
        val observer = store.observeState(actions).test()
        actions.onNext(
            Action.LoadBeers
        )
        assertEquals(true, observer.values().last().loading)
    }

    @Test
    fun `state is updated when error event is emitted`() {
        val observer = store.observeState(actions).test()
        val error = UnknownHostException()
        actions.onNext(Action.Error(error))
        val lastValue = observer.values().last()
        assertEquals(false, lastValue.loading)
        assertEquals(error, lastValue.error)
    }

}