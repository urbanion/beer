package com.holoceo.beers.domain.list

import com.holoceo.beers.BeerMocks
import com.holoceo.beers.domain.BeerRepository
import io.reactivex.Observable
import io.reactivex.Single
import org.junit.Before
import org.junit.Test
import org.mockito.kotlin.any
import org.mockito.kotlin.mock
import org.mockito.kotlin.whenever
import java.net.UnknownHostException

class MiddlewareTest {
    private val beerRepo = mock<BeerRepository>()
    private lateinit var middleWare: Middleware

    @Before
    fun setUp() {
        middleWare = Middleware(beerRepo)
        whenever(beerRepo.getBeers(any(), any())).thenReturn(Single.just(BeerMocks.BEERS_1))
    }

    @Test
    fun `first page is loaded without user actions`() {
        val actions = Observable.never<Action>()
        val state = State()
        val observer = middleWare.process(actions, { state }).test()
        observer.assertValues(Action.BeersLoaded(BeerMocks.BEERS_1, false))
    }

    @Test
    fun `no more beers can be loaded if result size is less than page size`() {
        val actions = Observable.never<Action>()
        val state = State()
        val observer = middleWare.process(actions, { state }).test()
        observer.assertValues(Action.BeersLoaded(BeerMocks.BEERS_1, false))
    }

    @Test
    fun `more beers can be loaded if result size is equal to page size`() {
        whenever(beerRepo.getBeers(any(), any())).thenReturn(Single.just(BeerMocks.BEERS_PAGE))
        val actions = Observable.never<Action>()
        val state = State()
        val observer = middleWare.process(actions, { state }).test()
        observer.assertValues(Action.BeersLoaded(BeerMocks.BEERS_PAGE, true))
    }

    @Test
    fun `beers are loaded when load action is emitted`() {
        val actions = Observable.just<Action>(Action.LoadBeers)
        val state = State()
        val observer = middleWare.process(actions, { state }).test()
        observer.assertValues(
            Action.LoadBeers,
            Action.BeersLoaded(BeerMocks.BEERS_1, false),
            Action.BeersLoaded(BeerMocks.BEERS_1, false)
        )
    }

    @Test
    fun `error action is emitted on loading error`() {
        val error = UnknownHostException()
        whenever(beerRepo.getBeers(any(), any())).thenReturn(Single.error(error))
        val actions = Observable.never<Action>()
        val state = State()
        val observer = middleWare.process(actions, { state }).test()
        observer.assertValues(Action.Error(error))
    }
}